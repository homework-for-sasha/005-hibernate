package com.example.demo.Entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NotNull
    @Size(min = 5, max = 30, message = "Поле 'userName' должно содержать от 5 до 30 символов.")
    private String userName;

    @NotNull
    @Size(min = 8, max = 30, message = "Поле 'password' должно содержать от 8 до 30 символов.")
    private String password;

    @Size (min = 5, max = 13)
    private String firstName;

}
