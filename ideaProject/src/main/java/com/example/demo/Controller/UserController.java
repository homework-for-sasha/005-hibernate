package com.example.demo.Controller;

import com.example.demo.Entity.User;
import com.example.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.UUID;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String getHome() {
        return "html/homePage";
    }

    @GetMapping("/getUsers")
    public String getUsers(Model model) {
        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);
        return "ftlh/userList";
    }

    @PostMapping(value = "/createUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public String createUser(User user) {
        userService.createUser(user);
        return "redirect:/getUsers";
    }

    @PostMapping(value = "/updateUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public String updateUser(Model model, User user) {
        User users = userService.updateUser(user);
        model.addAttribute("users", users);
        return "redirect:/getUsers";
    }

    @GetMapping("/deleteUser")
    public String deleteUser(String uuid) {
        userService.deleteUserById(UUID.fromString(uuid));
        return "redirect:/getUsers";
    }

}
