package com.example.demo.Service;

import com.example.demo.Entity.User;

import java.util.List;
import java.util.UUID;

public interface UserService {
    public User createUser(User user);

    public void deleteUser(User user);

    public List<User> getAllUsers();

    public User getUSerById(UUID id);

    public User updateUser(User user);

    public void deleteUserById(UUID id);
}

